const keys = (obj) => {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys

  if (!obj) return [];

  const toReturn = [];

  for (let key in obj) {
    toReturn.push(key);
  }

  return toReturn;
};

module.exports = keys;
