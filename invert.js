function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert

  if (!obj) return {};

  const toReturn = {};

  for (let key in obj) {
    toReturn[obj[key]] = key;
  }

  return toReturn;
}

module.exports = invert;
