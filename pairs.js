const pairs = (obj) => {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs

  if (!obj) return [];

  const toReturn = [];

  for (let key in obj) {
    toReturn.push([key, obj[key]]);
  }

  return toReturn;
};

module.exports = pairs;
