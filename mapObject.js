const mapObject = (obj, cb) => {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject

  if (!obj || !cb) return {};

  const toReturn = { ...obj };

  for (let key in obj) {
    toReturn[key] = cb(obj[key], key);
  }

  return toReturn;
};

module.exports = mapObject;
