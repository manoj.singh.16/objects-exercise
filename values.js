const values = (obj) => {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values

  if (!obj) return [];

  const toReturn = [];
  for (let key in obj) {
    toReturn.push(obj[key]);
  }

  return toReturn;
};

module.exports = values;
