const defaults = (obj, defaultProps) => {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults

  if (!obj) return {};
  if (!defaultProps) return obj;

  for (let key in defaultProps) {
    if (!obj[key]) {
      obj[key] = defaultProps[key];
      break;
    }
  }

  return obj;
};

module.exports = defaults;
